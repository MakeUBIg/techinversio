<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144822_create_mub_user_album extends Migration
{
    public function getTableName()
    {
        return 'mub_user_album';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'position' => 'position',
            'title' => 'title',
            'mub_user_id'  =>  'mub_user_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'title' => $this->string(50)->notNull(),
            'description' => $this->string(100)->notNull(),
            'visible' => "enum('0','1')",
            'position' => $this->integer(1),
            'slug' => $this->string(),
            'type' => "enum('personal','gallery','slider') NOT NULL DEFAULT 'gallery'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
