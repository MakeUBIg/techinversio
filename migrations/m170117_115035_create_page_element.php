<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_115035_create_page_element extends Migration
{
    public function getTableName()
    {
        return 'page_elements';
    }
    public function getForeignKeyFields()
    {
        return [
            'page_id' => ['mub_user_page', 'id'],
            'element_id' => ['mub_element','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'element_id' => 'element_id',
            'page_id'  =>  'page_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'element_id' => $this->integer(11)->defaultValue(NULL),
            'page_id' => $this->integer(11)->defaultValue(NULL),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
