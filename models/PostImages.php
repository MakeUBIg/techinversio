<?php

namespace app\models;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use Yii;

/**
 * This is the model class for table "post_images".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $post_id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $thumbnail_url
 * @property string $thumbnail_path
 * @property string $full_path
 * @property string $visible
 * @property string $keyword
 * @property integer $width
 * @property integer $height
 * @property integer $display_width
 * @property integer $display_hieght
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Post $post
 */
class PostImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'post_id', 'width', 'height', 'display_width', 'display_hieght'], 'integer'],
            [['url'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['visible', 'type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at','url'], 'safe'],
            [['title'], 'string', 'max' => 50],
            [['description', 'thumbnail_url', 'thumbnail_path', 'full_path'], 'string', 'max' => 100],
            [['keyword'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'thumbnail_url' => Yii::t('app', 'Thumbnail Url'),
            'thumbnail_path' => Yii::t('app', 'Thumbnail Path'),
            'full_path' => Yii::t('app', 'Full Path'),
            'visible' => Yii::t('app', 'Visible'),
            'keyword' => Yii::t('app', 'Keyword'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
            'display_width' => Yii::t('app', 'Display Width'),
            'display_hieght' => Yii::t('app', 'Display Hieght'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create_post'] = [];
        $scenarios['update_post'] = [];
        return $scenarios;
    }


    public function checkEmpty($attribute,$params)
    {
        $image = UploadedFile::getInstances($this, $attribute);
        if(Yii::$app->controller->action->id == 'update')
        {
            $id = Yii::$app->request->getQueryParam('id');
            $data = self::find()->where(['post_id'=>$id,'del_status'=>'0'])->all();
            if(empty($data))
            {
               if(empty($image) || $image==null)
               {
                $this->$attribute = null;
                $this->addError($attribute, 'You must upload atleast one Image');
               }
            }
        }
        else if(Yii::$app->controller->action->id == 'create')
        {
            if(empty($image) || $image==null)
            {
             $this->$attribute = null;
             $this->addError($attribute, 'You must upload atleast one Image');
            }
        }
    } 

    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
