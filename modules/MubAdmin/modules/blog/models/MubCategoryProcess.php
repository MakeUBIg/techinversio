<?php 
namespace app\modules\MubAdmin\modules\blog\models;

use app\models\MubCategory;
use app\components\Model;

class MubCategoryProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $mubCategory = new MubCategory();
        $mubCategory->scenario = 'create_category';
        $this->models = [
            'mubCategory' => $mubCategory
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $mubCategory = $model;
        $mubCategory->scenario = 'update_category';
        $this->relatedModels = [
            'mubCategory' => $mubCategory
        ];
        return $this->relatedModels;
    }

    public function saveCategory($mubCategory)
    {
    	return ($mubCategory->save()) ? $mubCategory->id : p($mubCategory->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['mubCategory']))
        {
        try {
        	$catId = $this->saveCategory($data['mubCategory']);
        	return ($catId) ? $catId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
