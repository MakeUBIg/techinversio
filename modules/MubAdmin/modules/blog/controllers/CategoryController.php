<?php
namespace app\modules\MubAdmin\modules\blog\controllers;
use Yii;
use app\models\MubCategory;
use app\components\MubController;
use app\models\MubCategorySearch;
use app\modules\MubAdmin\modules\blog\models\BlogProcess;
use app\modules\MubAdmin\modules\blog\models\MubCategoryProcess;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CategoryController extends MubController
{
   public function getPrimaryModel()
   {
        return new MubCategory();
   }

   public function getProcessModel()
   {
        return new MubCategoryProcess();
   }

   public function getSearchModel()
   {
        return new MubCategorySearch();
   }
}
