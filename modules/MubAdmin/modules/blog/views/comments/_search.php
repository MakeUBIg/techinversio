<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $postComment app\postComments\PostCommentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-comment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($postComment, 'id') ?>

    <?= $form->field($postComment, 'post_id') ?>

    <?= $form->field($postComment, 'email') ?>

    <?= $form->field($postComment, 'name') ?>

    <?= $form->field($postComment, 'comment_id') ?>

    <?php // echo $form->field($postComment, 'title') ?>

    <?php // echo $form->field($postComment, 'type') ?>

    <?php // echo $form->field($postComment, 'comment_text') ?>

    <?php // echo $form->field($postComment, 'mub_user_id') ?>

    <?php // echo $form->field($postComment, 'status') ?>

    <?php // echo $form->field($postComment, 'approved_on') ?>

    <?php // echo $form->field($postComment, 'approved_by') ?>

    <?php // echo $form->field($postComment, 'created_at') ?>

    <?php // echo $form->field($postComment, 'updated_at') ?>

    <?php // echo $form->field($postComment, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
