<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\MubCategory;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html>
<head>
<title>Home :: TechInversio | Business Blog</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<?= Html::csrfMetaTags() ?>
<?php $this->head();?>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
<?php $this->beginBody() ?>
<div class="header">
<div class="header-top">
    <div class="container">
        <div class="logo">
            <a href="/"><h1>TECH INVERSIO</h1></a>
        </div>
        <div class="search">
            <form action="/blog/search" method="get">
                <input type="text" name="s" id="top-search">
                <input type="submit" value="">
            </form>
        </div>
        <div class="social">
            <ul class="social-right" style="margin-right: 1em!important">
                <li><a href="https://www.facebook.com/Inversio-Solutions-LLP-1995501887339283/?ref=bookmarks" class="facebook" target="_blank"> </a></li>
                <li><a href="https://twitter.com/login" class="facebook twitter" target="_blank"> </a></li>
                <li><a href="https://plus.google.com/" class="facebook chrome" target="_blank"> </a></li>
                <li><a href="https://www.linkedin.com/company-beta/13336594" class="facebook in" target="_blank"> </a></li>
                
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
 <input type="hidden" name="fb_script" value="fb_script" id="fb_script">
<div class="head-bottom">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav col-md-12">
        <li class="<?= ($this->params['page'] == 'home') ? 'active' :'' ;?>"><a href="/">Home</a></li>
        
        <li class="<?= ($this->params['page'] == 'blogs') ? 'active' :'' ;?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blogs <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <?php
          $allCategory = MubCategory::find()->where(['del_status' => '0'])->all();
                foreach($allCategory as $mubCategory)
                {
                ?>
                <li><a href="/blog/category?name=<?= $mubCategory->category_name;?>"><?= $mubCategory->category_name;?></a></li>
              <?php }?>
          </ul>
        </li>
        
        <li class="<?= ($this->params['page'] == 'contact') ? 'active' :'' ;?>"><a href="/site/contact">Contact Us</a></li>

        <li class='pull-right dropdown'>
        <?php
        if(Yii::$app->user->isGuest)
        {
        ?>
        <a href="<?= Url::to('/mub-admin')?>"><span class="glyphicon glyphicon-user"></span>  Login</a><?php
        } 
        else 
        {
        ?>
        <a class="dropdown-toggle" data-toggle="dropdown" role="button"> <span class="glyphicon glyphicon-user"></span><?= Yii::$app->user->identity->username ?></a>
        <ul class="dropdown-menu" style="margin-top: 0px">
            <li><a href="/mub-admin/blog/blogitem/create">Add Blog</a></li>
            <li><a href="/mub-admin">Dashboard</a></li>
            <li><a href="<?= Url::to(['/site/logout'])?>" data-method="post" >Logout</a></li>
        </ul>
        </li>
        <?php
        }
        ?>
        </li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
<!--head-bottom-->
</div>
<?php
    NavBar::begin([
        'brandLabel' => '<div class="logo">
            <a href="/"><h1>TECH INVERSIO</h1></a>
        </div>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $allCategories = MubCategory::find()->where(['del_status' => '0'])->all();
    $blogCategories = [];
    foreach ($allCategories as $category) 
    {
        $blogCategories[] =  ['label' => $category->category_name, 'url' => '/blog/category?name='.$category->category_slug,'options'=>['class'=>'dropdown-color']];
    }
    echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Blogs', 'items' => $blogCategories,],
        ['label' => 'Contact', 'url' => ['/site/contact']],

        Yii::$app->user->isGuest ? (
        ['label' => 'Login', 'url' => ['/site/login']]
         ) : (
         ['label' => Yii::$app->user->identity->username, 'items' => [
         ['label' => 'Add Blog', 'url' => '/mub-admin/blog/blogitem/create','options'=>['class'=>'dropdown-color']],
         ['label' => 'Dashboard', 'url' => '/mub-admin/','options'=>['class'=>'dropdown-color']],
         ['label' => 'Logout', 'url' => 'site/logout','options'=>['class'=>'dropdown-color']],
        ],]
            )
    ],
]);
    NavBar::end();
    ?>
    <?= $content ?>
<div class="footer dark-bg">
    <div class="container dark-bg">
        <div class="col-md-4 footer-left">
            <h6>CONTACT</h6>
            <p>Email: mk@inversiosolutions.com</p>
        </div>
        <div class="col-md-4 footer-middle">
        <!--<h4>Twitter Feed</h4>
        <div class="mid-btm">
            <p>Consectetur adipisicing</p>
            <p>Sed do eiusmod tempor</p>
            <a href="https://MakeUBig.com/">https://MakeUBig.com/</a>
        </div>
        
            <p>Consectetur adipisicing</p>
            <p>Sed do eiusmod tempor</p>
            <a href="https://MakeUBig.com/">https://MakeUBig.com/</a>
    -->
        </div>
        <div class="col-md-4 footer-right">
            <h4>Quick Links</h4>
            <li><a href="/">Home</a></li>
            <li><a href="/blog">Blogs</a></li>
            <li><a href="/site/contact">Contact</a></li>
           <li> <?php
        if(Yii::$app->user->isGuest)
        {
        ?>
        <a href="<?= Url::to('/mub-admin')?>"><span class="glyphicon glyphicon-user"></span>  Login</a><?php
        } 
        else 
        {
        ?>
        <a href="/mub-admin"><?= Yii::$app->user->identity->username ?></a><?php
        }
        ?>
        </li>
            
        </div>
        <div class="clearfix" style="background: #121212"></div>
    </div>
</div>
<div class="foot-nav">
</div>
<div class="clearfix" style="background: #121212"></div>
<div class="copyright">
    <div class="container">
        <p>© 2016 Business_Blog. All rights reserved | Template by <a href="http://MakeUBig.com/">MakeUBig</a></p>
    </div>
</div>
<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>
