<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\helpers\ImageUploader;
$postImages = new \app\models\PostImages();
 $signupMail = new \app\models\SignupMail();
$postComment = new \app\models\PostComment();
$allPosts = $postModel::find()->where(['del_status' => '0'])->orderBy(['id' => SORT_DESC])->limit(4)->all();
?>
<div class="technology-1">
<div class="container">
<div class="col-md-9 technology-left">
  <div class="business">
    <div id="contact" class="contact">
    <h3>Contact</h3>        
     <div class="contact-grids co">
     <div class="contact-icons  col-md-12 text-center">
            <div class="contact-grid" style="width: 100%; float: center;">
                <div class="contact-fig2">
                    <span class="glyphicon glyphicon-envelope2" aria-hidden="true"></span>
                </div>
                <p><a href="mailto:info@example.com">mk@inversiosolutions.com</a></p>
            </div>
            <div class="clearfix"> </div>
         </div>
            <?php $form = ActiveForm::begin([
              'id' => 'contact-mail'
              ]); ?>
                <?= $form->field($contactMail, 'name')->textInput(['autofocus' => true,'class' => 'form-control']);?>
                <?= $form->field($contactMail, 'email')->textInput(['class' => 'form-control']);?>
                <?= $form->field($contactMail, 'message')->textArea(['class' => 'form-control', 'rows' => 6 ]);?>
                <input type="submit" value="SEND">
             <?php ActiveForm::end(); ?>
             </div>          
    </div>
</div>
</div>
<!-- technology-right -->
 <?php echo $this->render('_right',['postModel' => $postModel]);?>
    <div class="clearfix"></div>
    <!-- technology-right -->
</div>
</div>