<?php 

use app\helpers\ImageUploader;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

    $postImages = new \app\models\PostImages();
    $postCategory = new \app\models\PostCategory();
    $mubCategory = new \app\models\MubCategory();
    $signupMail = new \app\models\SignupMail();
    $postComment = new \app\models\PostComment();

 ?>    
<div class="banner">
    <div class="container">
        <h2>A Tech-information platform</h2>
        <p>To add value to Knowledge & skills for professionals</p>
        <a href="/blog">READ ARTICLE</a>
    </div>
</div>
<!-- technology -->
<div class="technology">
    <div class="container">
        <div class="col-md-9 technology-left">
        <div class="tech-no">
        <?php 
        $allPosts = $postModel::find()->where(['del_status' => '0','status' => 'active'])->orderBy(['id' => SORT_DESC])->limit(4)->all();
        foreach($allPosts as $post){
            $postDetail = $post->postDetail;
            $currentCategory = $postCategory::find()->where(['post_id' => $post->id,'del_status' => '0'])->one();
            $commentCount = $postComment::find()->where(['post_id' => $post->id,'del_status' => '0'])->andWhere(['<>','approved_by','NULL'])->count();
                $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
            ?>
            <!-- technology-top -->
            <div class="soci">
                <ul>
                    <li><a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/blog/post-detail?id='.$post->url;?>" class="facebook-1 chrome" target="_blank"></a>
                    </li>
                    <li>
                    <a href="https://twitter.com/share?text=<?=$post->post_title;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/blog/post-detail?id='.$post->url;?>"
                          class="facebook-1" target="_blank"></a></li>
                   <li><a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/blog/post-detail?id='.$post->url;?>" class="facebook-1 twitter" target="_blank"> </a></li>
                </ul>
            </div>
             <div class="tc-ch">
                
                <?php if(!$postImage){
                    if($currentCategory->category_id != NULL){
                        ?>
                <a class="positon blog blue" href="<?= '/blog/post-detail?id='.$post->url;?>"><?=$currentCategory->category->category_name;}}?></a>

                <?php if($postImage){
                    if($currentCategory->category_id != NULL){?>
                <a class="blog blue" href="<?= '/blog/post-detail?id='.$post->url;?>"><?=$currentCategory->category->category_name;?></a>
                <div class="tch-img">
                    <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="<?= '/'.ImageUploader::resizeRender($postImage->url, '700', '350');?>" class="img-responsive" alt="<?=$post->post_title;?>"/></a>
                </div>
                <?php }else{?>

                    <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="<?= '/'.ImageUploader::resizeRender($postImage->url, '700', '350');?>" class="img-responsive" alt="<?=$post->post_title;?>"/></a>
                <?php }}?>
                <h3><a href="<?= '/blog/post-detail?id='.$post->url;?>"><?=$post->post_title;?></a></h3>
                <p><?= $post->post_excerpt;?></p>

                <div class="blog-poast-info">
                    <ul>
                    <?php $mubUser = $mubUserModel::findOne($post->mub_user_id);
                    ?>
                        <li><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#"> <?= $mubUser->username;?> </a></li>
                        <li><i class="glyphicon glyphicon-calendar"> </i><?= $post->created_at;?></li>
                        <li><i class="glyphicon glyphicon-eye-open"> </i>Total Reads: <?= $postDetail->read_count;?></li>
                        <li><i class="glyphicon glyphicon-pencil"> </i>Total Comment: <?= ($commentCount == 0) ? 'No Comments' : $commentCount;?></li> 
                    </ul>
                </div>

            </div>

            <div class="clearfix"></div>
            <?php }?>
            </div>
        </div>

        <!-- technology-right -->
        <?php echo $this->render('_right',['postModel' => $postModel]);?>
        <div class="clearfix"></div>   </div>
</div>
<a href="/blog" class="btn btn-primary show" style="text-align: center; width: 25%; background-color: #FA4B2A"><b>Show More Blogs</a>