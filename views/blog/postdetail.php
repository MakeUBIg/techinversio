<?php 
$postComment = new \app\models\PostComment();
 $postCategory = new \app\models\PostCategory();
    $mubCategory = new \app\models\MubCategory();
$currentCategory = $postCategory::find()->where(['post_id' => $post->id,'del_status' => '0'])->one();
use app\models\Post;
use app\helpers\ImageUploader;
use yii\widgets\ActiveForm;
	$this->title = 'Title from view';
	$this->title = $this->title ? $this->title : 'default title';

/* facebook meta tag*/
	$this->registerMetaTag([
    	'title' => 'og:title',
    	'content' => $post->post_title,	
	]);

	$this->registerMetaTag([
    	'app_id' => 'fb:app_id',
    	'content' => '1872999093024677'
	]);

	$this->registerMetaTag([
    	'type' => 'og:type',
    	'content' => 'article'
	]);

	$this->registerMetaTag([
    	'url' => 'og:url',
    	'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
	]);
	if($postImage){
		$this->registerMetaTag([
	    	'image' => 'og:image',
	    	'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads'.$postImage->url,
		]);
	}
	$this->registerMetaTag([
    	'description' => 'og:description',
    	'content' => 'description here'
	]);

/*close facebook meta tag*/

/*twitter meta tag*/

	$this->registerMetaTag([
    	'card' => 'twitter:card',
    	'content' => "summury"
	]);
	$this->registerMetaTag([
    	'site' => "twitter:site",
    	'content' => "@publisher_handle"
	]);
	
	$this->registerMetaTag([
    	'title' => 'twitter:title',
    	'content' => 'page title'
	]);

	$this->registerMetaTag([
    	'description' => 'twitter:description',
    	'content' => 'Page description less than 200 characters'	
	]);

	$this->registerMetaTag([
    	'creater' => 'twitter:creater',
    	'content' => '@author_handle'	
	]);

	$this->registerMetaTag([
    	'url' => 'twitter:url',
    	'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
	]);
	if($postImage){
	$this->registerMetaTag([
	    	'image' => 'twitter:image:src',
	    	'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads'.$postImage->url,
		]);
	}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
			<div class="business">
				<div class=" blog-grid2">
				 <?php if(!$postImage){
                    if($currentCategory->category_id != NULL){?>
                    <a class="positon blog blue" href="<?= '/blog/post-detail?id='.$post->url;?>"><?=$currentCategory->category->category_name;}}?></a>
                    <?php if($postImage){
                        if($currentCategory->category_id != NULL){?>
                    <a class="blog blue" href="<?= '/blog/post-detail?id='.$post->url;?>"><?=$currentCategory->category->category_name;?></a>
					<img src="<?= ($postImage) ? '/'.ImageUploader::resizeRender($postImage->url, '700', '300') : 'https://placeholdit.imgix.net/~text?txtsize=61&txt=650%C3%97300&w=650&h=300';?>" class="img-responsive" alt="">
					<?php }else{?>

                    <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="<?= '/'.ImageUploader::resizeRender($postImage->url, '700', '350');?>" class="img-responsive" alt="<?=$post->post_title;?>"/></a>
                <?php }}?>
					<div class="blog-text">
						<h5><?=$post->post_title;?></h5>
						<?= $postDetail->post_content; ?>			
					</div>
				</div>

				<div class="blog-poast-info">
                    <ul class="col-lg-12">
                        <li class="col-lg-3"><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#"> <?= $mubUser->username;?> </a></li>
                        <li class="col-lg-3"><i class="glyphicon glyphicon-calendar"> </i><?= $post->created_at;?></li>
                        <li class="col-lg-3"><i class="glyphicon glyphicon-eye-open"> </i>Total Reads: <?= $postDetail->read_count;?></li>
                        <li class="col-lg-3"><i class="glyphicon glyphicon-pencil"> </i>Comment: <?= ($comments['comments_count'] == 0) ? 'No ' : $comments['comments_count'];?></li> 
                    </ul>
                 </div>

		<h4>Share On Social Media</h4>
				<div class="col-md-4" style="border-bottom: 1px solid #aeaeae!important; margin-bottom: 1.5em;">
				 <a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/blog/post-detail?id='.$post->url;?>" class="btn btn-primary faceb" target="_blank">
				 <i class="fa fa-facebook-square" aria-hidden="true" style="padding-right: 0.2em"></i>Facebook</a>
				</div>

                <div class="col-md-4" style="border-bottom: 1px solid #aeaeae!important; margin-bottom: 1.5em;"><a href="https://twitter.com/share?text=<?=$post->post_title;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/blog/post-detail?id='.$post->url;?>" class="btn btn-primary twitt" target="_blank">
                <i class="fa fa-twitter-square" aria-hidden="true" style="padding-right: 0.2em;"></i>Twitter</a>
                 </div>

                <div class="col-md-4" style="border-bottom: 1px solid #aeaeae!important; margin-bottom: 1.5em;"><a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/blog/post-detail?id='.$post->url;?>" class="btn btn-primary gplus" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true" style="padding-right: 0.2em"></i>Google<b>+</b>
                </a>
                </div>
                          
				<?php if($comments['comments']){?>

				<h2>Comments</h2>
				<?php foreach($comments['comments'] as $cmt){?>
				<div class="comment-top">
				<div class="media-left">
				  <a href="#">
					<img src="/images/si.png" alt="" data-pin-nopin="true">
				  </a>
				</div>
					<div class="media-body">
					  	<h4 class="media-heading"><?=$cmt->name;?></h4>
						<p class="show-less<?=$cmt->id;?>"><?php if(strlen($cmt->comment_text) > 255){
						echo substr($cmt->comment_text, 0,250); ?>
						<a style="cursor: pointer;" onclick="readmore(<?=$cmt->id;?>)"> read more...</a>
						<?php } else{
							echo $cmt->comment_text;
							}?></p>
							<p class="show-more<?=$cmt->id;?>" style="display:none;">
								<?=$cmt->comment_text;?>
								<a style="cursor: pointer;" onclick="readless(<?=$cmt->id;?>)"> show less...</a>
							</p>
					</div>
				</div>
					<?php }?>
				<?php }?>
				<div class="comment">
					<h3>Leave a Comment</h3>
					<div class=" comment-bottom">
					<?php $form = ActiveForm::begin(['id' => 'user_comment']);?>
							<?= $form->field($postComment, 'name')->textInput(['maxlength'=> 250]) ?>
							<?= $form->field($postComment, 'email')->textInput(['maxlength'=> 250]) ?>
							<?= $form->field($postComment, 'comment_text')->textArea(['cols'=>'6']) ?>
							<?= $form->field($postComment, 'url')->hiddenInput(['value' => \Yii::$app->request->get('id')])->label(false) ?>
							<input type="submit" value="Post">
						<?php ActiveForm::end();?>
					</div>
				</div>
			</div>
		</div>
		<!-- technology-right -->
		<?php echo $this->render('_right',['postModel' => new Post()]);?>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>