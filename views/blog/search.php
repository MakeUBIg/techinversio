<?php 
use app\helpers\ImageUploader;
use app\models\Post;
use yii\widgets\LinkPager;
$postImages = new \app\models\PostImages();
$allBlogs = Post::find()->where(['status' => 'active','del_status' => '0'])->all();
?>
<style>
	#paginat ul li
	{
		display: inline;
	}
	#paginat ul li a
	{
		line-height: inherit;
	}
</style>
<div class="banner1">	
</div>
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
			<div class="business">

			<?php 
				if(!empty($blogs)){
			foreach($blogs as $blogDetail){	
				$blog = Post::findOne($blogDetail->post_id);
				$postImage = $postImages::find()->where(['post_id' => $blog->id])->one();
				?>
				<div class="rev-1">
				<?php if($postImage){?>
					<div class="rev-img">
						<a href="/blog/post-detail?id=<?= $blog->url; ?>"><img src="<?= ($postImage) ? '/'.ImageUploader::resizeRender($postImage->url, '282', '132') : 'https://placeholdit.imgix.net/~text?txtsize=61&txt=282%C3%97300&w=282&h=132';?>" class="img-responsive" alt=""/></a>
					</div>
					<?php }?>
					<div class="<?=($postImage) ? 'rev-info' : ''?>">
						<h3><a href="/blog/post-detail?id=<?= $blog->url; ?>"><?= $blog->post_title;?></a></h3>
						<p><?= $blog->post_excerpt;?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php }?>
				<div id="paginat">
				<center> <?= LinkPager::widget([
                   'pagination' => $pages,
                   ]);
                  ?></center>
                  </div>
				<?php }else{?>
				<div>
					<h3><a>No Blogs found</a></h3>
						<p>Sorry ! But we were not able to find any blog containing your Search String</p>
						<p>Following are some blogs you may be Interested In</p>
				</div>
				<div class="clearfix"></div>
				<?php }?>
			</div>
		</div>
		<!-- technology-right -->
		<?php echo $this->render('_right',['postModel' => new Post()]);?>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>